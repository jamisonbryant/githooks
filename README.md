# GitHooks
A compilation of useful hooks for interacting with git repositories

## Installation
1. `git clone` the repository
2. `cd` to your repository
2. `cp` the hook you want to your repo's `.git/hooks`

### How do I know the correct directory?

Hooks are stored in a directory named after _what they should be named 
when copied to the target repo._ In other words, a hook called 
`ensure_time_and_ticket.hook` in the `commit-msg` directory should be
copied to your repo's `.git/hooks/commit-msg`. **You can only have one
hook per type active at a time.**

## Dependencies

All hooks are implemented in Python. Python 3 is required to use these 
hooks.

## Further Reading

* [Git Hooks (3rd-party site)](https://githooks.com)
* [Git Manual](https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks)
* [Git Hooks (Atlassian Git Tutorial)](https://www.atlassian.com/git/tutorials/git-hooks)
